.POSIX:
.SUFFIXES:


PKGNAME=ypkg2-tools
MODULE=gitlab.com/serebit/ypkg2-tools

VERSION="0.0.0"

PREFIX?=/usr/local
BINDIR?=$(DESTDIR)$(PREFIX)/bin

GO?=go
GOFLAGS?=

GOSRC!=find . -name "*.go"
GOSRC+=go.mod go.sum

# missing in some non-GNU make
RM?=rm -f


build/ypkg2-tools: $(GOSRC)
	$(GO) build $(GOFLAGS) \
	-ldflags "-X $(MODULE)/cli.VersionCode=$(VERSION)" \
	-o $@


all: build/ypkg2-tools

check:
	$(GO) install github.com/securego/gosec/cmd/gosec@latest
	$(GO) install honnef.co/go/tools/cmd/staticcheck@latest
	$(GO) fmt -x ./...
	$(GO) vet ./...
	${GOPATH}/bin/gosec -exclude=G204 ./...
	${GOPATH}/bin/staticcheck ./...

clean:
	$(GO) mod tidy
	$(RM) -r build


.DEFAULT_GOAL := all

.PHONY: all check clean
