package core

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"strings"
)

type YamlMap = map[string]interface{}
type YamlSlice = yaml.MapSlice

type Yaml struct {
	Map    YamlMap
	Slice  YamlSlice
	Keys   []string
	String string
	Lines  []string
}

func newYaml() *Yaml {
	return &Yaml{make(YamlMap), YamlSlice{}, nil, "", nil}
}

func UnmarshalYaml(file string) (*Yaml, error) {
	if !FileExists(file) {
		return nil, fmt.Errorf("no %s file found", file)
	}

	bytes, err := ReadFile(file)
	if err != nil {
		return nil, fmt.Errorf("failed to read %s: %s", file, err)
	}

	y := newYaml()

	err = yaml.Unmarshal(bytes, &y.Map)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal %s: %s", file, err)
	}

	err = yaml.Unmarshal(bytes, &y.Slice)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal %s: %s", file, err)
	}

	for _, it := range y.Slice {
		y.Keys = append(y.Keys, it.Key.(string))
	}

	y.String = string(bytes)

	y.Lines = strings.Split(y.String, "\n")

	return y, nil
}
