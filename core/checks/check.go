package checks

import "gitlab.com/serebit/ypkg2-tools/core"

const (
	CheckPass = iota
	CheckSkip
	CheckFail
)

type Check struct {
	Desc string
	Task func(y *core.Yaml) CheckResult
}

type CheckResult struct {
	Status int
	Errs   []error
}

func skip() CheckResult {
	return CheckResult{CheckSkip, nil}
}

func fail(errs ...error) CheckResult {
	return CheckResult{CheckFail, errs}
}

var Checks = []Check{empty, required, order, homepage}
