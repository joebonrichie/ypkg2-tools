package checks

import (
	"fmt"
	"gitlab.com/serebit/ypkg2-tools/core"
)

var requiredKeys = []string{
	"name", "version", "release",
	"source",
	"license", "component",
	"summary", "description",
}

var required = Check{
	Desc: "File should contain required keys",
	Task: func(y *core.Yaml) (result CheckResult) {
		var missing []string
		for _, it := range requiredKeys {
			if !core.Contains(y.Keys, it) {
				missing = append(missing, it)
			}
		}

		if len(missing) != 0 {
			result = fail(fmt.Errorf("the following keys are missing: %v", missing))
		}

		return result
	},
}
