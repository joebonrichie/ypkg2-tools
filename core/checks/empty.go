package checks

import (
	"fmt"
	"gitlab.com/serebit/ypkg2-tools/core"
)

var empty = Check{
	Desc: "File should not be empty",
	Task: func(y *core.Yaml) (result CheckResult) {
		if len(y.Slice) == 0 {
			result = fail(fmt.Errorf("no keys present in file"))
		}

		return result
	},
}
